Just a collection of my C Programs
---

[![Build Status](https://travis-ci.org/crazyuploader/C.svg?branch=master)](https://travis-ci.org/crazyuploader/C) [![Build status](https://ci.appveyor.com/api/projects/status/h76fgqjc90oxt4x9?svg=true)](https://ci.appveyor.com/project/crazyuploader/c)

[`DataStructure`](/DataStructure) directory contains Data Structure header files which are used in [`main.c`](main.c) using C.

<b>main.c</b>
* All programs are merged and called using `switch` function.
* Keeps taking input until user enters invalid input.
